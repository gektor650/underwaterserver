const SERVO_ZERO = 1500;
// var pin2 = 0;
const Gpio = require('pigpio').Gpio;

const servo1 = new Gpio(22, {mode: Gpio.OUTPUT});
const servo2 = new Gpio(10, {mode: Gpio.OUTPUT});

const servo3 = new Gpio(9, {mode: Gpio.OUTPUT});
const servo4 = new Gpio(27, {mode: Gpio.OUTPUT});


servo1.servoWrite(SERVO_ZERO);
servo2.servoWrite(SERVO_ZERO);
servo3.servoWrite(SERVO_ZERO);
servo4.servoWrite(SERVO_ZERO);

module.exports.engines = {
  leftEsc: (value) => {
    servo1.servoWrite(value);
  },
  rightEsc: (value) => {
    servo2.servoWrite(value);
  },
  topLeftEsc: (value) => {
    servo3.servoWrite(value);
  },
  topRightEsc: (value) => {
    servo4.servoWrite(value);
  }
}
