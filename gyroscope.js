const i2c = require('i2c-bus');
const MPU6050 = require('i2c-mpu6050');

const address = 0x68;
const i2c1 = i2c.openSync(1);
const sensor = new MPU6050(i2c1, address);

var reading = false;
var sensorData = {};

module.exports.gyroscope = {
  getData : () => {
    return JSON.stringify(sensorData);
  },
  readRotation : () => {
    sensor.readRotation(function (err, rot) {
      if (err) {
        console.log(e);
        return;
      }
      console.log(rot);
    });
  },
  readSensor : () => {
  	if (!reading) {
  		reading = true;
  	}
  	var start = new Date().getTime();
  	sensor.read(function (err, data) {
      reading = false;
  		if (err) {
  			console.log(err);
  		} else {
  			var time = new Date().getTime() - start;
  			var msg = JSON.stringify(data);
        sensorData = data;
  		}
  	});
  }
}
