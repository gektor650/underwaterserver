
const Gpio = require('pigpio').Gpio;
const rele1 = new Gpio(4, {mode: Gpio.OUTPUT});
const rele2 = new Gpio(17, {mode: Gpio.OUTPUT});

module.exports.rele = {
  rele1On: () => {
    rele1.digitalWrite(1);
  },
  rele1Off: () => {
    rele1.digitalWrite(0);
  },
  rele2On: () => {
    rele2.digitalWrite(1);
  },
  rele2Off: () => {
    rele2.digitalWrite(0);
  }
}
