#!/bin/bash

base="/usr/share/nginx/html/"

cd $base

raspivid -n -w 720 -h 405 -fps 25 -vf -t 86400000 -b 1800000 -ih -o - \
| ffmpeg -y \
    -i - \
    -c:v copy \
    -c:a aac -q:a 2 \
    -map 0:0 \
    -f ssegment \
    -segment_time 4 \
    -segment_wrap 20 \
    -segment_format mpegts \
    -segment_list "/usr/share/nginx/html/stream.m3u8" \
    -segment_list_size 720 \
    -segment_list_flags live \
    -segment_list_type m3u8 \
    "%08d.ts"


trap "rm stream.m3u8 *.ts" EXIT

# vim:ts=2:sw=2:sts=2:et:ft=sh
