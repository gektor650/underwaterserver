const Gpio = require('pigpio').Gpio;

const light = new Gpio(11, {mode: Gpio.OUTPUT});

const LIGHT_MIN = 0;
const LIGHT_MAX = 255;

const LIGHT_DIAPASONE = LIGHT_MAX - LIGHT_MIN;

light.pwmWrite(LIGHT_MIN);

module.exports.light = {
  on: () => {
    light.pwmWrite(LIGHT_MAX);
  },
  off: () => {
    light.pwmWrite(LIGHT_MIN);
  },
  set: (val) => {
    if(val < 0 || val > 100) return;
    val = Math.round(LIGHT_MIN + LIGHT_DIAPASONE / 100 * val);
    console.log("Light:"+val);
    light.pwmWrite(val);
  }
}
