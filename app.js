
'use strict';
const express = require('express');
const net = require('net');
const app = express();
const controller = require('./controller.js').controller;
const rele = require('./rele.js').rele;
const gyroscope = require('./gyroscope.js').gyroscope;

const exec = require('child_process').exec;

const tempCheckCommand = "cat /sys/class/thermal/thermal_zone0/temp";
const PING = "PING";
const PONG = "PONG\r\n";
const DATA_COMMAND = "C:D;"
const SENSOR_COMMAND = "C:S;"
const TEMPERATURE = "T:";
const DELIMITER = ";";
const COMMAND_PREFIX = "C:";
const END = "\r\n";
var tempVal = "";
var sensorVal = "";
/////
var releCheck = true;

setInterval(function () {
  var child = exec(tempCheckCommand, function (error, stdout, stderr) {
    if (error !== null) {
      console.log('exec error: ' + error);
    } else {
      var temp = parseFloat(stdout)/1000;
      tempVal = DATA_COMMAND + TEMPERATURE + temp + DELIMITER + END;
    }
  });
  gyroscope.readSensor();
  sensorVal = SENSOR_COMMAND + gyroscope.getData() + DELIMITER + END;
}, 1000);

const server = net.createServer((socket) => {
  socket.on('data', (data) => {
    var command = data.toString();
    if(command.startsWith(PING)) {
      socket.write(sensorVal + tempVal + PONG);
    }
    if(command.startsWith(COMMAND_PREFIX)) {
      controller.parse(command);
    }
  });
  // socket.end('goodbye\n');
}).on('error', (err) => {
  console.log(err);
  // handle errors here
  // throw err;
});
server.timeout = 0;
// grab an arbitrary unused port.
server.listen(49655, () => {
  console.log('opened server on', server.address());
});
