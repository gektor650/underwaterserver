const NEW_LINE = "\n";
const COMMAND_DELIMITER = ";";
const VALUE_DELIMITER = ":";
const ARG_COMMAND = "C";
const ARG_ANGLE = "A";
const ARG_VALUE = "V";
const ARG_LIGHT = "LIGHT";
const COMMAND_TYPE_L = "L";
const COMMAND_TYPE_R = "R";

const SERVO_ZERO = 1500;
const SERVO_DIAPASON = 400;
const SERVO_STEP = 25;
const SERVO_MAX = 1900;
const SERVO_MIN = 1100;

const engines = require('./engines.js').engines;
const light = require('./light.js').light;

function Data() {
  var lastPwmVal = SERVO_ZERO;
  var currentVal = 0;
}

const leftData = new Data();
const rightData = new Data();
const topLeftData = new Data();
const topRightData = new Data();

setInterval(function(){
  module.exports.controller.proceed(module.exports.controller.useLeft, leftData);
  module.exports.controller.proceed(module.exports.controller.useRight, rightData);
  module.exports.controller.proceed(module.exports.controller.useTopLeft, topLeftData);
  module.exports.controller.proceed(module.exports.controller.useTopRight, topRightData);
}, 200);

function getTargetPwm(value) {
  var target = SERVO_ZERO + (SERVO_DIAPASON / 100 * value);
  return Math.floor(target/SERVO_STEP) * SERVO_STEP;
}

module.exports.controller = {
  useLeft: (value) => {
     console.log("LEFT FREQ: " + value);
     engines.leftEsc(value);
  },
  useRight: (value) => {
     console.log("RIGHT FREQ: " + value);
     engines.rightEsc(value);
  },
  useTopLeft: (value) => {
     console.log("TOP LEFT FREQ: " + value);
     engines.topLeftEsc(value);
  },
  useTopRight: (value) => {
     console.log("TOP RIGHT FREQ: " + value);
     engines.topRightEsc(value);
  },
  parse : (commandString) => {
    var commandsString = commandString.split(NEW_LINE);
    for(var j = 0; j < commandsString.length; j++) {
      commandString = commandsString[j];
      if(commandString.length < 6) continue;
      if(commandString.startsWith(ARG_COMMAND)) {
          var commands = commandString.split(COMMAND_DELIMITER);
          if(commands.length >= 3) {
              var angle = 0, value = 0;light = -1;
              var commandType;
              for (var i = 0; i < commands.length; i++) {
                  if (commands[i]) {
                      //We will receive command in [0], value in [1]
                      var cVArray = commands[i].split(VALUE_DELIMITER);
                      if (cVArray.length == 2) {
                          switch (cVArray[0]) {
                              case ARG_ANGLE:
                                  angle = cVArray[1];
                                  break;
                              case ARG_VALUE:
                                  value = cVArray[1];
                                  break;
                              case ARG_COMMAND:
                                  commandType = cVArray[1];
                                  break;
                          }
                      }
                  }
              }
              if(commandType == ARG_LIGHT) {
                light.set(value);
              } else if(commandType) {
                module.exports.controller.use(commandType, angle, value);
              }
          }
      }
    }
  },
  revertCoeffIfNeed : (coeff) => {
    if(coeff < 0.5) {
      coeff = 1 - coeff;
      return - coeff;
    }
    return coeff;
  },
  use : (position, angle, value) => {
   // var convertedVal = SERVO_MIN + (SERVO_STEP * value);
   if(value <= 100 && value >= 0) {
     var leftCoeff = 1;
     var rightCoeff = 1;
     if(angle < 90 && angle >= 0) {
       rightCoeff = 1 / 90 * angle;
     } else if(angle < 180 && angle > 90){
       var an2 = angle - 90;
       leftCoeff = 1 - 1 / 90 * an2;
     } else if(angle > -90 && angle < 0) {
       rightCoeff = 1 / 90 * (-angle);
     } else if(angle > -180 && angle < -90){
       var an2 = (-angle) - 90;
       leftCoeff = 1 - 1 / 90 * an2;
     } else if(angle == -180 || angle == 180) {
       leftCoeff = 0;
     }
     leftCoeff = module.exports.controller.revertCoeffIfNeed(leftCoeff);
     rightCoeff = module.exports.controller.revertCoeffIfNeed(rightCoeff);
     console.log("leftCoeff: " + leftCoeff + " rightCoeff: " + rightCoeff);
     if(position == COMMAND_TYPE_L) {
       if(angle >= 0) {
         leftData.currentVal = (-value * leftCoeff);
         rightData.currentVal = -(-value * rightCoeff);
       } else {
         leftData.currentVal = (value * leftCoeff);
         rightData.currentVal = -(value * rightCoeff);
       }
     } else if(position == COMMAND_TYPE_R) {
       if(angle >= 0) {
         topLeftData.currentVal = (-value * leftCoeff);
         topRightData.currentVal = -(-value * rightCoeff);
       } else {
         topLeftData.currentVal = (value * leftCoeff);
         topRightData.currentVal = -(value * rightCoeff);
       }
     }
   }
  },
  proceed : (method, data) => {
   var lastPwmVal = data.lastPwmVal;
   var currentVal = data.currentVal;
   var value = SERVO_ZERO;
   var targetPwm = getTargetPwm(currentVal);
   if(currentVal != 0) {
       if(targetPwm < SERVO_ZERO && lastPwmVal > SERVO_ZERO
         || targetPwm > SERVO_ZERO && lastPwmVal < SERVO_ZERO) {
           value = SERVO_ZERO;
       } else if(lastPwmVal < targetPwm){
         value = lastPwmVal + SERVO_STEP;
       } else if(lastPwmVal > targetPwm) {
         value = lastPwmVal - SERVO_STEP;
       } else if(lastPwmVal == targetPwm) {
         value = lastPwmVal;
       }
   }
   if(value > SERVO_MAX) {
     value = SERVO_MAX;
   } else if(value < SERVO_MIN){
     value = SERVO_MIN;
   }
   if(data.lastPwmVal != value) {
     data.lastPwmVal = value;
     method(value);
   }
  }
}
